import VueVideoPlayer from 'vue-video-player'
import 'video.js/dist/video-js.css'

export default async ({ Vue }) => {
  Vue.use(VueVideoPlayer, {
  })
}
