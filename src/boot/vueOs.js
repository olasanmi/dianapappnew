import Vue from 'vue'
import * as VueAos from 'vue-aos'

export default async () => {
  Vue.use(VueAos)
}
