import RouterPush from 'src/utils/routerPush'
import Vue from 'vue'

export default () => {
  Vue.prototype.$push = RouterPush
}
