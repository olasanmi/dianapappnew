import formatPrice from 'src/utils/globalsFunction'
import Vue from 'vue'

export default () => {
  Vue.prototype.$price = formatPrice
}
