import lineClamp from 'vue-line-clamp'

export default async ({ Vue }) => {
  Vue.use(lineClamp, {})
}
