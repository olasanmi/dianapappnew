import Vue from 'vue'
import vueVimeoPlayer from 'vue-vimeo-player'

export default async () => {
  Vue.use(vueVimeoPlayer)
}
