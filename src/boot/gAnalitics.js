import Vue from 'vue'
import VueAnalytics from 'vue-analytics'

export default async () => {
  Vue.use(VueAnalytics, {
    id: 'UA-142838098-1'
  })
}
