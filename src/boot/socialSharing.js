import Vue from 'vue'
import VueSocialSharing from 'vue-social-sharing'

export default async () => {
  Vue.use(VueSocialSharing)
}
