import VueNumber from 'vue-number-animation'

export default async ({ Vue }) => {
  Vue.use(VueNumber)
}
