import axios from 'axios'

import { domain } from './data'

const url = domain

export function getFeeds (callBack, errorCallBack) {
  axios({ url: url + '/api/v1/feeds', headers: { 'Accept': 'application/json; charset=utf-8' }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}
