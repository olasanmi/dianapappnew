import axios from 'axios'

import { domain } from './data'

const url = domain

export function getCounting (callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/counting', headers: { 'Accept': 'application/json; charset=utf-8', 'Authorization': `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function getCountingAll (callBack, errorCallBack) {
  axios({ url: url + '/api/v1/counting-all', headers: { 'Accept': 'application/json; charset=utf-8' }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function add (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/counting', data: data, headers: { 'Accept': 'application/json; charset=utf-8', 'Authorization': `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function updateCounting (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/update/counting', data: data, headers: { 'Accept': 'application/json; charset=utf-8', 'Authorization': `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function delCounting (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/counting/' + data, headers: { 'Accept': 'application/json; charset=utf-8', 'Authorization': `Bearer ${newToken}` }, method: 'DELETE' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}
