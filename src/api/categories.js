import axios from 'axios'

import { domain } from './data'

const url = domain

export function getCategory (callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/category', headers: { 'Accept': 'application/json; charset=utf-8', 'Authorization': `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function setCategory (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/category', data: data, headers: { 'Accept': 'application/json; charset=utf-8', 'Authorization': `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function delCategory (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/category/' + data, headers: { 'Accept': 'application/json; charset=utf-8', 'Authorization': `Bearer ${newToken}` }, method: 'DELETE' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function getFromtCategory (callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/list/categories', headers: { 'Accept': 'application/json; charset=utf-8' }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}
