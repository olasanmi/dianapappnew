import axios from 'axios'

import { domain } from './data'

const url = domain

export function setPost (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/post', data: data, headers: { 'Accept': 'application/json; charset=utf-8', 'Content-Type': 'multipart/form-data', 'Authorization': `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function listPost (callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/post', headers: { 'Accept': 'application/json; charset=utf-8', 'Authorization': `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function destroyPost (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/post/' + data, headers: { 'Accept': 'application/json; charset=utf-8', 'Authorization': `Bearer ${newToken}` }, method: 'DELETE' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function updatePost (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/post/' +  data.id, data: data.data, headers: { 'Accept': 'application/json; charset=utf-8', 'Content-Type': 'multipart/form-data', 'Authorization': `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function addPostImage (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/posts/images', data: data, headers: { 'Accept': 'application/json; charset=utf-8', 'Content-Type': 'multipart/form-data', 'Authorization': `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function delPostImage (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/posts/images/' + data, headers: { 'Accept': 'application/json; charset=utf-8', 'Authorization': `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function listFrontPost (callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/list/post', headers: { 'Accept': 'application/json; charset=utf-8' }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function filterPost (id, callBack, errorCallBack) {
  axios({ url: url + '/api/v1/post/filters/' + id, headers: { 'Accept': 'application/json; charset=utf-8' }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function likePost (data, callBack, errorCallBack) {
  axios({ url: url + '/api/v1/posts/like', data: data, headers: { 'Accept': 'application/json; charset=utf-8' }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function commentPost (data, callBack, errorCallBack) {
  axios({ url: url + '/api/v1/posts/comments', data: data, headers: { 'Accept': 'application/json; charset=utf-8' }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}
