
const routes = [
  {
    path: '/',
    component: () => import('layouts/main.vue'),
    children: [
      { path: '/', component: () => import('pages/index.vue') },
      { path: '/servicios', component: () => import('pages/services.vue') },
      { path: '/pagos', component: () => import('pages/paymentMethods.vue') },
      { path: '/pagos/confirmado/:ref', component: () => import('pages/payConfirm.vue') },
      { path: '/agendamiento', component: () => import('pages/appoinment.vue') },
      { path: '/blog', component: () => import('pages/blogs.vue') },
      { path: '/contacto', component: () => import('pages/contact.vue') },
      { path: '/casos-exito', component: () => import('pages/successStories.vue') },
      { path: '/blog/entrada/:id', component: () => import('pages/blogsPost.vue') },
      { path: '/politicas-cambio', component: () => import('pages/changesAndRefound.vue') },
      { path: '/tratamiento-datos', component: () => import('pages/personalData.vue') },
      { path: '/vacantes', component: () => import('pages/vacancie.vue') },
      // services route
      { path: '/diseño-sonrisa', component: () => import('pages/serviceFilter.vue') },
      { path: '/minidiseno', component: () => import('pages/serviceFilter.vue') },
      { path: '/fase-higienica', component: () => import('pages/serviceFilter.vue') },
      { path: '/cirugias', component: () => import('pages/serviceFilter.vue') },
      { path: '/ortodoncia', component: () => import('pages/serviceFilter.vue') },
      { path: '/lentes', component: () => import('pages/serviceFilter.vue') },
      { path: '/blanqueamiento', component: () => import('pages/serviceFilter.vue') },
      { path: '/limpieza', component: () => import('pages/serviceFilter.vue') }
    ]
  },
  {
    path: '/auth',
    component: () => import('layouts/main.vue'),
    children: [
      { path: 'login', component: () => import('pages/auth/login.vue') }
    ]
  },
  {
    path: '/dashboard',
    component: () => import('layouts/admin/main.vue'),
    children: [
      {
        path: 'main',
        component: () => import('pages/admin/index.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'case-real',
        component: () => import('pages/admin/caseReal/index.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'case-real/add',
        component: () => import('pages/admin/caseReal/add.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'case-success',
        component: () => import('pages/admin/caseSuccess/index.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'case-success/add',
        component: () => import('pages/admin/caseSuccess/add.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'newsletter',
        component: () => import('pages/admin/newsletter/index.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'blogs',
        component: () => import('pages/admin/blogs/index.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'blogs/add',
        component: () => import('pages/admin/blogs/add.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'payments',
        component: () => import('pages/admin/payments/index.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'contador',
        component: () => import('pages/admin/counting/index.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'contador/add',
        component: () => import('pages/admin/counting/add.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'vacantes',
        component: () => import('pages/admin/vacancies/index.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'vacantes/add',
        component: () => import('pages/admin/vacancies/add.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'vacantes/add/:id',
        component: () => import('pages/admin/vacancies/add.vue'),
        meta: {
          requiresAuth: true
        }
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/error404.vue')
  }
]

export default routes
