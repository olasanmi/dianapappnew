import { setPayment } from 'src/api/payment'

export function sendPay ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return setPayment(data, (response) => {
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function setPayments ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('setPayAmount', data)
    resolve(data)
  })
}

export function setFilter ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('setFilter', data)
    resolve(data)
  })
}
