export function setPosts (blog, data) {
  blog.posts = data
  blog.allPosts = data
}

export function setPostsFilter (blog, data) {
  blog.filterPost = data
}

export function setCategories (blog, data) {
  blog.categories = data
}

export function filterAllPost (blog, id) {
  if (id === 0) {
    blog.posts = blog.allPosts
    return
  }
  blog.posts = blog.allPosts.filter(data => {
    return data.category.id === parseInt(id)
  })
}

export function setFeeds (blog, data) {
  blog.feeds = data
}
