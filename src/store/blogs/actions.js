// categories import request
import { getCategory, setCategory, delCategory, getFromtCategory } from 'src/api/categories'

// categories actions
export function listCategories ({ commit }) {
  return new Promise((resolve, reject) => {
    return getCategory((response) => {
      commit('setCategories', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function addCategories ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return setCategory(data, (response) => {
      commit('setCategories', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function destroyCategories ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return delCategory(data, (response) => {
      commit('setCategories', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function listFrontCategories ({ commit }) {
  return new Promise((resolve, reject) => {
    return getFromtCategory((response) => {
      commit('setCategories', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

// post import request
import { setPost, listPost, destroyPost, updatePost, addPostImage, delPostImage, listFrontPost, commentPost } from 'src/api/post'

export function addPost ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return setPost(data, (response) => {
      commit('setPosts', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function editPost ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return updatePost(data, (response) => {
      commit('setPostsFilter', {})
      commit('setPosts', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function getPost ({ commit }) {
  return new Promise((resolve, reject) => {
    return listPost((response) => {
      commit('setPosts', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function getFrontPost ({ commit }) {
  return new Promise((resolve, reject) => {
    return listFrontPost((response) => {
      commit('setPosts', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function deletePost ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return destroyPost(data, (response) => {
      commit('setPosts', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function filterPost ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('setPostsFilter', data)
    resolve()
  })
}

export function setPostImages ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return addPostImage(data, (response) => {
      commit('setPosts', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function destroyPostImage ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return delPostImage(data, (response) => {
      commit('setPosts', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function filterPostForCat ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('filterAllPost', data)
    resolve()
  })
}

export function commentBlogPost ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return commentPost(data, (response) => {
      commit('setPostsFilter', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function filterFeeds ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('setFeeds', data)
    resolve()
  })
}
