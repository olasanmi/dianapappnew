export function posts (blog) {
  return blog.posts
}

export function allPosts (blog) {
  return blog.allPosts
}

export function filterPost (blog) {
  return blog.filterPost
}

export function categories (blog) {
  return blog.categories
}

export function feeds (blog) {
  return blog.feeds
}
