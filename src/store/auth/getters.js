export function user (user) {
  return user.user
}

export function token (user) {
  return user.token
}

export function locale (user) {
  return user.locale
}

export function filterService (user) {
  return user.filterService
}

export function global (user) {
  return user.loadedGlobal
}
