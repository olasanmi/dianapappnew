const formatPrice = function (total) {
  const val = (total / 1).toFixed(0).replace('.', ',')
  return '' + val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
}

export default formatPrice
